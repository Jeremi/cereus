#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "utils.h"

void test_str_nappend(void)
{
    char *str1 = "Hello";
    char *str2 = " world";
    char *str3 = "!";

    char *buffer = NULL;

    str_nappend(&buffer, str1, strlen(str1));
    assert(strcmp(str1, buffer) == 0);

    str_nappend(&buffer, str2, strlen(str2));
    assert(strncmp(str1, buffer, strlen(str1)) == 0);
    assert(strcmp(str2, buffer+strlen(str1)) == 0);

    str_nappend(&buffer, str3, strlen(str3));
    assert(strncmp(str1, buffer, strlen(str1)) == 0);
    assert(strncmp(str2, buffer+strlen(str1), strlen(str2)) == 0);
    assert(strcmp(str3, buffer+strlen(str1)+strlen(str2)) == 0);

    free(buffer);
}

int main(void)
{
    test_str_nappend();
}
