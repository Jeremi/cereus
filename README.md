Cereus - Moteur de template léger
=================================
Cereus est un moteur de template léger.
Il peut s'utiliser en ligne de commande, à la manière de filtre UNIX, ou bien via un bibliothèque C.

Installation
------------
    $ make install

Exemple
-------
Utilisation en ligne de commande à la façon dun filtre
    $ template.html > cereus -d content.dic > output.html

Dictionaire
-----------
Le fichier représentant le dictionnaire doit contenir une liste de clé/valeur. Il n'y à qu'une seule syntaxe pour le moment :
    key1: value
    key2:
	multiline
	value
	...

