#include <stdlib.h>
#include <string.h>

#include "utils.h"


/* Concatenation avec reallocation eventuelle */
void str_nappend(char **dst, const char *src, size_t n)
{
    if (*dst == NULL) {
	*dst = malloc(sizeof(char)*(n+1));
	if (*dst == NULL) return;
	*dst[0] = '\0';
    } else {
	size_t dstlen = strlen(*dst);
	/* XXX Mauvaise portabilitée, sizeof(*dst) à diviser avec sizeof(char) */
	if (n < sizeof(*dst) - dstlen) {
	    *dst = realloc(*dst, sizeof(char)*(dstlen+n));
	    if (*dst == NULL) return;
	}
    }
    strncat(*dst, src, n);
}
