#ifndef UTILS_H
#define UTILS_H

/* Concatenation avec reallocation eventuelle */
void str_nappend(char **dst, const char *src, size_t n);

#endif
