#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <stdlib.h>

typedef struct KeyVal {
    char *key;
    char *val;
    struct KeyVal *next;
} KeyVal;

typedef struct Dictionary {
    size_t size;
    KeyVal *list;
    KeyVal **tab;
} Dictionary;


/* Libère le dictionaire */
void dic_free(Dictionary *dic);

/* Recherche une valeur à partir de la clé */
char * dic_get(const Dictionary *dic, char *key);

/* Parse un fichier et fabrique un dictionaire */
Dictionary * dic_parse(char *path);


#endif
