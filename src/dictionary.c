#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h> /* isblank */
#include <string.h>

#include "dictionary.h"
#include "utils.h"


static int dic_elemCpr(const void *e1, const void *e2);
static int dic_addElem(Dictionary *dic, const char *line);
static void dic_makeTab(Dictionary *dic);
Dictionary * dic_readFile(FILE *file);


/**********************
 * Fonctions visibles *
 **********************/

/* Libère le CONTENU du dictionaire */
    void
dic_free(Dictionary *dic)
{
    if (dic == NULL) return;
    KeyVal *elem;

    while (dic->list != NULL) {
	elem = dic->list;
	dic->list = dic->list->next;
	free(elem->key);
	free(elem->val);
	free(elem);
    }
    if (dic->tab != NULL) {
	free(dic->tab);
    }
    free(dic);
}

/* Recherche une valeur à partir de la clé */
    char *
dic_get(const Dictionary *dic, char *key)
{
    KeyVal keyElem;
    keyElem.key = key;
    KeyVal *ptr = &keyElem;
    KeyVal *elem = bsearch(&ptr, dic->tab, dic->size, sizeof(KeyVal *), dic_elemCpr);
    if (elem == NULL || *(KeyVal **)elem == NULL) {
	return NULL;
    }
    return (*(KeyVal **)elem)->val;
}

/* Parse un fichier et fabrique un dictionaire */
    Dictionary *
dic_parse(char *path)
{
    FILE * dicfile;
    dicfile = fopen(path, "r");
    if (dicfile == NULL) {
	return NULL;
    }
    Dictionary *dic = dic_readFile(dicfile);
    fclose(dicfile);
    dic_makeTab(dic);
    return dic;
}



/**************************
 * Fonctions non-visibles *
 **************************/

/* Fonction de comparaison d'éléments */
    static int
dic_elemCpr(const void *e1, const void *e2)
{
    return strcmp((*(KeyVal **)e1)->key, (*(KeyVal **)e2)->key);
}

/* Ajout d'un élément dans la liste du dictionaire à partir d'une ligne*/
    static int
dic_addElem(Dictionary *dic, const char *line)
{
    char *chars_sep = " \t:=";
    char *chars_end = "\n\r\0";

    // Recherche du séparateur
    char *sep = strpbrk(line, chars_sep);
    if (sep != NULL && sep != line) {
	// Construction de la cle
	size_t cle_length = sep - line;
	char *cle = malloc(sizeof(char)*(cle_length+1));
	if (cle == NULL) return -1;
	strncpy(cle, line, cle_length);
	cle[cle_length] = '\0';

	// Ajout de l'element au dictionaire
	KeyVal *elem = malloc(sizeof(KeyVal));
	if (elem == NULL) return -1;
	elem->key = cle;
	elem->val = malloc(sizeof(char));
	if (elem->val == NULL) return -1;
	elem->val[0] = '\0';
	elem->next = dic->list;
	dic->list = elem;
	dic->size++;
    } else {
	// Suite de la valeur précédente
	if (dic->list == NULL || dic->list->key == NULL) {
	    // Erreur, il n'y a pas de valeur precedente
	    return 0;
	}
	chars_sep = " \t";
	sep = line;
    }

    // Construction de la valeur
    char *val = sep + strspn(sep, chars_sep);
    size_t val_length = strpbrk(val, chars_end) - val;
    if (val_length != 0 && val[0] != '\n') {
	// Ajout de la valeur au dictionaire
	if (dic->list->val != NULL && dic->list->val[0] != '\0') {
	    str_nappend(&(dic->list->val), "\n", 1);
	    if (dic->list->val == NULL) return -1;
	}
	str_nappend(& (dic->list->val), val, val_length);
	if (dic->list->val == NULL) return -1;
    }

    return 1;
}

/* Fabrique le tableau trié par ordre alphabetique */
    static void
dic_makeTab(Dictionary *dic)
{
    KeyVal *elem;
    unsigned int i;

    // fabrication du tableau
    dic->tab = malloc(sizeof(KeyVal *) * dic->size);
    if (dic->tab == NULL) return;
    for (elem = dic->list, i=0; (elem != NULL) && (i < dic->size); elem = elem->next, i++) {
	dic->tab[i] = elem;
    }

    // Tri du tableau
    qsort(dic->tab, dic->size, sizeof(KeyVal *), dic_elemCpr);
}

/* Lecture du fichier pour parser le dictionaire */
    Dictionary *
dic_readFile(FILE *file)
{
    char *chaine = NULL;
    size_t taille = 0;
    ssize_t retour;
    Dictionary *dic = malloc(sizeof(Dictionary));
    if (dic == NULL) return NULL;
    dic->size = 0;
    dic->list = NULL;
    dic->tab = NULL;

    while (1) {
	retour = getline(&chaine, &taille, file);
	if (retour == -1) {
	    break;
	}
	retour = dic_addElem(dic, chaine);
	if (retour == 0) {
	    dic_free(dic);
	    dic = NULL;
	    break;
	} else if (retour == -1) {
	    perror("malloc");
	    exit(EXIT_FAILURE);
	}
    }
    free(chaine);
    return dic;
}
