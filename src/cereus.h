#ifndef CEREUS_H
#define CEREUS_H

#include <stdio.h>

#include "dictionary.h"

typedef struct CEREUS {
    Dictionary *dic;
    char *prefix;
    char *suffix;
} CEREUS;


/* Initialise une nouvelle instance */
CEREUS * cereus_create(const char *dic_path, const char *prefix, const char *suffix);

/* Parse */
char * cereus_parse(CEREUS *cereus, const char *tpl);

/* Libère une instance */
void cereus_free(CEREUS *cereus);

#endif
