#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
    #ifdef OPTIONS_LONGUES
#include <getopt.h>
    #endif

#include "cereus.h"

/* Définition des valeurs par défaut */
#define INPUT_DEFAUT "stdin"
#define OUTPUT_DEFAUT "stdout"
#define DIC_DEFAUT ""
#define PRE_DEFAUT "{{"
#define SUF_DEFAUT "}}"

/* Nom des variables d'environements */
#define INPUT_ENV "CEREUS_INPUT"
#define OUTPUT_ENV "CEREUS_OUTPUT"
#define DIC_ENV "CEREUS_DIC"
#define PRE_ENV "CEREUS_PREFIX"
#define SUF_ENV "CEREUS_SUFFIX"


void affiche_aide(char *nom_prog)
{
    fprintf(stderr, "Usage: %s [options]\n", nom_prog);
    fprintf(stderr, "Options:\n"
	    "-h"
#ifdef OPTIONS_LONGUES
	    ", --help\t"
#endif
	    "\t\tThis help screen \n"
	    "-i <FILE>"
#ifdef OPTIONS_LONGUES
	    ", --input <FILE>"
#endif
	    "\tInput template file \n"
	    "-o <FILE>"
#ifdef OPTIONS_LONGUES
	    ", --output <FILE>"
#endif
	    "\tOuput result file \n"
	    "-d <FILE>"
#ifdef OPTIONS_LONGUES
	    ", --dic <FILE>\t"
#endif
	    "\tDictionary file \n"
	    "-p <PREFIX>"
#ifdef OPTIONS_LONGUES
	    ", --prefix <PREFIX>"
#endif
	    "\tKeyword prefix \n"
	    "-s <SUFFIX>"
#ifdef OPTIONS_LONGUES
	    ", --suffix <SUFFIX>"
#endif
	    "\tKeyword suffix \n"
	   );
}

    void
run_template(char *input_path, char *output_path, char *dic_path, char *prefix, char *suffix)
{
    FILE *input_file;
    FILE *output_file;
    CEREUS *cereus;

    char *input = NULL;
    char *output = NULL;
    size_t taille = 0;

    // Creation cereus
    cereus = cereus_create(dic_path, prefix, suffix);
    if (cereus == NULL) {
	fprintf(stderr, "Erreur d'iitialisation\n");
	exit(EXIT_FAILURE);
    }

    // Ouverture du fichier template
    if (strcmp(input_path, "stdin") == 0) {
	input_file = stdin;
    } else {
	input_file = fopen(input_path, "r");
	if (input_file == NULL) {
	    perror("fopen");
	    exit(EXIT_FAILURE);
	}
    }
    
    // Ouverture du fichier destination
    if (strcmp(output_path, "stdout") == 0) {
	output_file = stdout;
    } else {
	output_file = fopen(output_path, "w");
	if (output_file == NULL) {
	    perror("fopen");
	    exit(EXIT_FAILURE);
	}
    }


    // Parsing template
    while (1) {
	if (getline(&input, &taille, input_file) == -1) {
	    break;
	}
	output = cereus_parse(cereus, input);
	if (output == NULL) {
	    perror("malloc");
	    exit(EXIT_FAILURE);
	}
	fprintf(output_file, "%s", output);
	free(output);
    }

    cereus_free(cereus);
    // TODO fclose
}

    char *
copy_env(const char *name, char *dst)
{
    char * retour_getenv = getenv(name);
    if ((retour_getenv != NULL) && (strlen(retour_getenv) != 0)) {
	dst = malloc(strlen(retour_getenv) + 1);
	if (dst == NULL) {
	    perror("malloc");
	    exit(EXIT_FAILURE);
	}
	strcpy(dst, retour_getenv);
    }
    return dst;
}

    int
main (int argc, char * argv[])
{
    /* Pour la copie des chaines d'environnement */
    char * env_input = NULL;
    char * env_output = NULL;
    char * env_dic = NULL;
    char * env_pre = NULL;
    char * env_suf = NULL;

    /* Variables contenant les valeurs effectives des paramètres */
    static char * input = INPUT_DEFAUT;
    static char * output = OUTPUT_DEFAUT;
    static char * dic = DIC_DEFAUT;
    static char * prefix = PRE_DEFAUT;
    static char * suffix = SUF_DEFAUT;

    int option;

    /* Lecture des variables d'environnement */
    if (copy_env(INPUT_ENV, env_input) != NULL) {
	input = env_input;
    }
    if (copy_env(OUTPUT_ENV, env_output) != NULL) {
	output = env_output;
    }
    if (copy_env(DIC_ENV, env_dic) != NULL) {
	dic = env_dic;
    }
    if (copy_env(PRE_ENV, env_pre) != NULL) {
	prefix = env_pre;
    }
    if (copy_env(SUF_ENV, env_suf) != NULL) {
	suffix = env_suf;
    }

    /* Lecture des options en ligne de commande */
    opterr = 1;
    while (1) {
#ifdef OPTIONS_LONGUES
	int index = 0;
	static struct option longopts[] = {
	    { "input",	1,  NULL,   'i' },
	    { "output",	1,  NULL,   'o' },
	    { "dic",	1,  NULL,   'd' },
	    { "prefix",	1,  NULL,   'p' },
	    { "suffix",	1,  NULL,   's' },
	    { "help",	0,  NULL,   'h' },
	    { NULL,	0,  NULL,   0 }
	};
	option = getopt_long(argc, argv, "i:o:d:p:s:h", longopts, &index);
#else
	option = getopt(argc, argv, "i:o:d:p:s:h");
#endif
	if (option == -1) {
	    break;
	}

	switch (option) {
	    case 'i' :
		input = optarg;
		break;
	    case 'o' :
		output = optarg;
		break;
	    case 'd' :
		dic = optarg;
		break;
	    case 'p' :
		prefix = optarg;
		break;
	    case 's' :
		suffix = optarg;
		break;
	    case 'h' :
		affiche_aide(argv[0]);
		exit(EXIT_SUCCESS);
	    default :
		break;
	}
    }

    /* Vérification des paramètres */
    if (input == NULL || strlen(input) == 0) {
	input = INPUT_DEFAUT;
    }
    if (output == NULL || strlen(output) == 0) {
	output = OUTPUT_DEFAUT;
    }
    if (dic == NULL || strlen(dic) == 0) {
	dic = DIC_DEFAUT;
    }
    if (prefix == NULL || strlen(prefix) == 0){
	prefix = PRE_DEFAUT;
    }
    if (suffix == NULL || strlen(suffix) == 0) {
	suffix = SUF_DEFAUT;
    }

    /* Lancement appli */
    run_template(input, output, dic, prefix, suffix); 

    return EXIT_SUCCESS;
}
