#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> /* isgraph */

#include "utils.h"
#include "dictionary.h"
#include "cereus.h"

/* Vérifie si le mot est un identifiant valide */
    static int
check_idt(const char *word,size_t length)
{
    unsigned int i;
    for (i = 0; i < length; i++) {
	if (!isgraph(word[i])) {
	    return 0;
	}
    }
    return 1;
}


/*
 * Cree une nouvelle instance
 */
    CEREUS *
cereus_create(const char *dic_path, const char *prefix, const char *suffix)
{
    CEREUS *cereus;
    size_t pre_len, suf_len;

    if ((dic_path == NULL) || (prefix == NULL) || (suffix == NULL)) {
	return NULL;
    }

    pre_len = strlen(prefix);
    suf_len = strlen(suffix);
    if ((pre_len == 0) || (suf_len == 0)) {
	return NULL;
    }
    
    cereus = malloc(sizeof(CEREUS));
    if (cereus == NULL) {
	return NULL;
    }
    cereus->dic = NULL;
    cereus->prefix = NULL;
    cereus->suffix = NULL;

    cereus->dic = dic_parse(dic_path);
    if (cereus->dic == NULL) {
	cereus_free(cereus);
	return NULL;
    }
    
    cereus->prefix = malloc(sizeof(char)*(pre_len+1));
    if (cereus->prefix == NULL) {
	cereus_free(cereus);
	return NULL;
    }

    cereus->suffix = malloc(sizeof(char)*(suf_len+1));
    if (cereus->suffix == NULL) {
	cereus_free(cereus);
	return NULL;
    }

    strcpy(cereus->prefix, prefix);
    strcpy(cereus->suffix, suffix);

    return cereus;
}

/* libere une instance */
    void
cereus_free(CEREUS *cereus)
{
    if (cereus == NULL) {
	return;
    }

    if (cereus->dic != NULL) {
	dic_free(cereus->dic);
    }

    if (cereus->prefix != NULL) {
	free(cereus->prefix);
    }

    if (cereus->suffix != NULL) {
	free(cereus->suffix);
    }

    free(cereus);
}

/* Parse */
    char *
cereus_parse(CEREUS *cereus, const char *input)
{
    char *prefix = cereus->prefix;
    char *suffix = cereus->suffix;
    Dictionary *dic = cereus->dic;
    char *output = malloc(sizeof(char));
    char *key = malloc(sizeof(char));
    if (output == NULL || key == NULL) return NULL;
    output[0] = '\0';
    size_t pre_length = strlen(prefix);
    size_t suf_length = strlen(suffix);
    size_t key_length;
    char *pre_pos;
    char *suf_pos;
    char *cur_pos = input;
    char *key_pos;
    char *value;

    while (1) {
	// Recherche du prefix
	pre_pos = strstr(cur_pos, prefix);
	if (pre_pos == NULL) {
	    break;
	}

	// Recherche du sufixe
	suf_pos = strstr(cur_pos+pre_length, suffix);
	if (suf_pos == NULL) {
	    break;
	}

	// Extraction clé
	key_pos = pre_pos + pre_length;
	key_length = suf_pos - key_pos;
	key[0] = '\0';
	str_nappend(&key, key_pos, key_length);

	// Verification clé
	if ((!check_idt(key, key_length)) || ((value = dic_get(dic, key)) == NULL)) {
	    // Ecrire jusqu'à la fin du sufixe et continuer
	    str_nappend(&output, cur_pos, suf_pos + suf_length - cur_pos);
	    cur_pos = suf_pos + suf_length;
	    continue;
	}

	// Ecriture
	str_nappend(&output, cur_pos, pre_pos-cur_pos);
	str_nappend(&output, value, strlen(value));
	cur_pos = suf_pos + suf_length;
    }

    free(key);
    str_nappend(&output, cur_pos, strlen(cur_pos));
    return output;
}
