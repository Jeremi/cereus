PREFIX=/usr/local

cereus:
	(cd src; $(MAKE))

install: cereus
	cp src/cereus $(PREFIX)/bin/
	cp doc/cereus.1 $(PREFIX)/share/man/man1/
